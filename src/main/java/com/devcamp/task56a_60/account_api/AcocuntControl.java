package com.devcamp.task56a_60.account_api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class AcocuntControl {
    @CrossOrigin  
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
       Account account1 = new Account("001", "Lan", 100000);
       Account account2 = new Account("002", "Que", 80000);
       Account account3 = new Account("003", "Phuong", 3000);
       
       System.out.println(account1.toString());
       System.out.println(account2.toString());
       System.out.println(account3.toString());

       account1.credit(800);
       account2.debit(80001);
       account3.credit(3000);

       System.out.println(account1.toString());
       System.out.println(account2.toString());
       System.out.println(account3.toString());

       account1.transferTo(account3, 4000);

       ArrayList<Account> accounts = new ArrayList<>();
       accounts.add(account1);
       accounts.add(account2);
       accounts.add(account3);

       return accounts;
    }

}
